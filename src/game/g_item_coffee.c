#include "header/local.h"

#define HEALTH_IGNORE_MAX 1 //copied from g_items.c

void
SP_item_coffee(edict_t *self)
{
	if (!self)
	{
		return;
	}

	if (deathmatch->value && ((int)dmflags->value & DF_NO_HEALTH))
	{
		G_FreeEdict(self);
		return;
	}

	self->model = "models/items/coffee/coffee.glb";
	self->count = 10;
	SpawnItem(self, FindItem("Health"));
	gi.soundindex("items/n_health.wav");
	self->style = HEALTH_IGNORE_MAX;
}