# Q2: Machine Reborn

Code repository for the Q2: Machine Reborn.

## How to play:

Well, the mod isn't out *or even finished* yet, so good luck with that.

## How to build

### Linux

0. You need `make` and a compiler like `gcc` or `clang`
1. Clone this repository
2. run `make`
3. Copy the produced `game.so` in the `q2mr` directory.

### Windows

TODO: same process as Linux, but with MinGW

### Mac

I honestly have no idea, if you do, please commit a PR.

## Credits

[id Software](https://github.com/id-Software/Quake-2) - for making Quake 2  
[Yamagi Quake 2](https://github.com/yquake2/yquake2) - for cleaning up and adding to the Quake 2 code   
[Spike](https://fte.triptohell.info/) - for creating FTE, the intended engine